<?php
/**
 * Created by PhpStorm.
 * User: wuliangbo
 * Date: 2019/11/19
 * Time: 09:05
 */

namespace think\filesystem\driver;

use League\Flysystem\FilesystemAdapter;
use think\filesystem\Driver;
use think\filesystem\Aliyun\OssAdapter;

class AliOss extends Driver
{
    protected function createAdapter(): FilesystemAdapter
    {
        return new OssAdapter($this->config['accessKey'],
            $this->config['secretKey'],
            $this->config['bucket'],
            $this->config['endpoint'],
            $this->config['isCName'],
        );
    }
}