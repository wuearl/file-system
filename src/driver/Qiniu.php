<?php
/**
 * @desc Created by PhpStorm
 * @author: wuearl
 * @since: 2019/11/23 3:53 下午
 */

namespace think\filesystem\driver;

use League\Flysystem\FilesystemAdapter;
use think\filesystem\Driver;
use think\filesystem\Qiniu\QiniuAdapter;

class Qiniu extends Driver
{
    protected function createAdapter(): FilesystemAdapter
    {
        return new QiniuAdapter(
            $this->config['accessKey'],
            $this->config['secretKey'],
            $this->config['bucket'],
            $this->config['domain']
        );
    }
}