<?php
/**
 * @desc Created by PhpStorm
 * @author: wuearl
 * @since: 2022/11/18 17:45
 */
namespace think\filesystem\Aliyun\Traits;

trait SignatureTrait
{
    /**
     * gmt.
     *
     * @param $time
     *
     * @return string
     *
     * @throws \Exception
     */
    public function gmt_iso8601($time)
    {
        // fix bug https://connect.console.aliyun.com/connect/detail/162632
        return (new \DateTime(null, new \DateTimeZone('UTC')))->setTimestamp($time)->format('Y-m-d\TH:i:s\Z');
    }
}